DOCKER = $(shell which docker)
BUILD_ARG = $(if $(filter  $(NOCACHE), 1),--no-cache)
BASE_PATH?="$(shell pwd)"
ENV_FILE?=.env
TEST_CONTAINER_NAME?={{ project_name }}-test
DEVELOPMENT_CONTAINER_NAME?={{ project_name }}

.PHONY: all {{ project_name }}_image {{ project_name }}_run_tests

{{ project_name }}_image:
	$(DOCKER) build $(BUILD_ARG) \
	-f build/package/app/Dockerfile \
	-t $(DEVELOPMENT_CONTAINER_NAME) .
{{ project_name }}_test_image:
	$(DOCKER) build $(BUILD_ARG) \
	-f build/package/test/Dockerfile \
	-t $(TEST_CONTAINER_NAME) .
run_function_tests:
	$(DOCKER) run \
    	--rm \
    	--env-file .env.test \
    	$(TEST_CONTAINER_NAME)
