import argparse
import sys

from jinja2 import Environment, FileSystemLoader
"""
Obtain a parser with the bare options
@:return argparse.ArgumentParser
"""


def get_parser() -> argparse.ArgumentParser:
    argument_parser = argparse.ArgumentParser(description='Create a skeleton template.')
    argument_parser.add_argument("-c",
                                 "--create",
                                 required=True,
                                 help="create a skeleton project"
                                 )
    argument_parser.add_argument("-p",
                                 "--project_name",
                                 required=True,
                                 help="This is the project name")
    subparsers = argument_parser.add_subparsers(help='sub-command help')
    parser_a = subparsers.add_parser('language',
                                     help='What kind of template you want to use')
    parser_a.add_argument('language', type=str, help='Determines which language to use')
    return argument_parser


if __name__ == "__main__":
    parser = get_parser()
    args = parser.parse_args()

    if args.create not in ('g', 'p'):
        sys.exit(1)
